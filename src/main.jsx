import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App.jsx";
import "./index.css";
import { ThemeProvider } from "./ThemeContext";
import { BrowserRouter, createBrowserRouter, RouterProvider } from "react-router-dom";
import { useTheme } from "./ThemeContext";

// const router = createBrowserRouter([
//   {
//     path: "/",
//     element: <App />,
//     errorElement:<ErrorPage/>
//   },
//   {
//     path: "/",
//     element: <App />,
//     errorElement:<ErrorPage/>,
//     loader={}
//   },
  
// ]);

// function main(){

  ReactDOM.createRoot(document.getElementById("root")).render(
    <ThemeProvider>
    <BrowserRouter><App /></BrowserRouter>
    </ThemeProvider>
  );
// }

