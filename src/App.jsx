import React from "react";
import Header from "./components/Header";
import Home from "./Home";
import { BrowserRouter as Router, Link, Route, Routes } from "react-router-dom";
import CountryDetails from "./components/CountryDetails";


function App() {
  return (
    <div>
      
      <Header />
      
      <Routes >
        <Route path="/" element={<Home />}/>
        <Route path="/country/:id" element={<CountryDetails/>}/>
      </Routes>
    </div>
  );
}

export default App;
