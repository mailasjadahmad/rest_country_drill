import React from "react";
// import "./Filter.css";
import { useTheme } from "../ThemeContext";
import { MenuItem,Box,InputLabel,FormControl,Select } from '@mui/material'
import { createTheme,ThemeProvider } from '@mui/material/styles';



function Filter({ onFilterChange }) {
  const { darkMode } = useTheme();
  const [region, setRegion] = React.useState("");
  const theme =createTheme({
    palette:{
      mode: darkMode?'dark':'light'
    }
  })
  function handlDropdownChange(event) {
    setRegion(event.target.value);

    // const { darkMode} = useTheme();
    const selectedValue = event.target.value;
    onFilterChange(selectedValue);
  }
  return (
    <ThemeProvider theme={theme}>
 <Box sx={{ minWidth: 150 }} >
      <FormControl fullWidth>
        <InputLabel id="demo-simple-select-label">Select Region</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={region}
          label="Select Region"
          onChange={handlDropdownChange}
        >
          <MenuItem value="All Region">All Region</MenuItem>
          <MenuItem value="Africa">Africa</MenuItem>
          <MenuItem value="Americas">Americas</MenuItem>
          <MenuItem value="Asia">Asia</MenuItem>
          <MenuItem value="Europe">Europe</MenuItem>
          <MenuItem value="Oceania">Oceania</MenuItem>
        </Select>
      </FormControl>
    </Box>
    </ThemeProvider>
   
  );
}
export default Filter;
