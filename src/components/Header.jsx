import React, { useState } from "react";
// import "./Header.css";
import { useTheme } from "../ThemeContext";

import DarkModeIcon from "@mui/icons-material/DarkMode";
import { Typography, Button } from "@mui/material";
import { createTheme, ThemeProvider } from "@mui/material/styles";

function Header() {
  const { darkMode, toggleTheme } = useTheme();
  const theme = createTheme({
    palette: {
      mode: darkMode ? "dark" : "light",
    },
  });

  const themeStyle = {
    backgroundColor: darkMode ? "#1e1e1e" : "white",
    color: darkMode ? "white" : "#1e1e1e",
  };
  const headerTheme = {
    backgroundColor: darkMode ? "#1e1e1e" : "white",
    color: darkMode ? "white" : "#1e1e1e",

    display: "flex",
    height: "80px",
    width: "100vw",
    justifyContent: "space-between",
    position: "fixed",
    left: "0",
    top: "0",
    backgroundColor: "var(--White)",
    padding: "20px 50px",
    boxShadow: "0px 2px 4px rgba(0, 0, 0, 0.1)",
  };

  // let mode="dark"
  return (
    <div className="header" style={headerTheme}>
      <ThemeProvider theme={theme} style={{ themeStyle }}>
        <Typography
          gutterBottom
          variant="h4"
          component="div"
          align="left"
          fontWeight="bold"
        >
          Where in the world?
        </Typography>
      </ThemeProvider>
      <Button variant="text" onClick={toggleTheme} style={{ themeStyle }}>
        <DarkModeIcon />
        Dark Mode
      </Button>
    </div>
  );
}

export default Header;
