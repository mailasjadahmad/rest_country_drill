import React from "react";
import { useTheme } from "../ThemeContext";
// import "./Flag.css";
// import { makeStyles } from '@mui/styles';
import { useNavigate } from "react-router-dom";
import { Card,CardContent,CardMedia,Typography } from '@mui/material'

// const useStyles = makeStyles({
//   card: {
//    width: 345, // Set the maximum width as needed

//   },
// });



function Flag(props) {
  // const classes = useStyles();
  const { darkMode } = useTheme();
  // console.log(props.capital[0])
  let navigate = useNavigate();
  return (
    <>
      <Card 
      className="card"
      sx={{width:275,
        borderRadius:4,
        boxShadow: '0 4px 6px rgba(0, 0, 0, 0.1)',
        cursor:"pointer",
      }}
        onClick={() => navigate(`/country/${props.id}`)}
      >
        <CardMedia
          component="img"
          alt={props.countryName}
          height="140"
          image={props.flagImg}
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="div" align="left">
            {props.countryName}
          </Typography>
          <Typography variant="body1" color="text.secondary">
            <b>Population:</b> {props.population}
          </Typography>
          <Typography variant="body1" color="text.secondary">
            <b>Region:</b> {props.region}
          </Typography>
          <Typography variant="body1" color="text.secondary">
            <b>Capital:</b> {props.capital}
          </Typography>
        </CardContent>
      </Card>
    </>
  );
}

export default Flag;
