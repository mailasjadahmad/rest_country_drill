import React, { useEffect, useState } from "react";
import { Navigate, useNavigate, useParams } from "react-router-dom";
// import "./CountryDetails.css";
import { useTheme } from "../ThemeContext";

function CountryDetails() {
  const [singleCountryData, setSingleCountryData] = useState("");
  const {darkMode}=useTheme();
  let { id } = useParams();
  let navigate = useNavigate();
  useEffect(() => {
    let url = `https://restcountries.com/v3.1/alpha/${id}`;
    fetch(url)
      .then((Response) => {
        if (!Response.ok) {
          throw new Error("Network response was not ok");
        }
        return Response.json();
      })
      .then((data) => {
        console.log(data);
        setSingleCountryData(data);
      });
  }, []);
  // console.log(singleCountryData[0].name.nativeName);
  return (
    <div className={`parent-container ${darkMode ? "dark-country" : ""}`} >
      
        {singleCountryData.length === 0 ? (
          <p>Loading...</p>
        ) : (
          <div className="country-details-outer">
        <button
          className="back"
          onClick={() => {
            navigate(-1);
          }}
        >
          Back
        </button>
          <div className="img-details-container">
            <img
              src={singleCountryData[0].flags?.png}
              alt={singleCountryData[0].name?.official}
              className="flag"
            />
            <div className="details-container">
              <h3 className="country-name">
                {singleCountryData[0].name?.official}
              </h3>
              <div className="details">
                <div className="details-left">
                  <p className="single-detail">
                    Natvie Name{" "}
                    <span>
                      {singleCountryData[0] ? (
                        Object.values(singleCountryData[0]?.name?.nativeName)[0]
                          .common
                      ) : (
                        <></>
                      )}
                    </span>
                  </p>
                  <p className="single-detail">
                    Population: <span>{singleCountryData[0].population}</span>
                  </p>
                  <p className="single-detail">
                    Region: <span>{singleCountryData[0].region}</span>
                  </p>
                  <p className="single-detail">
                    Sub Region: <span>{singleCountryData[0].subregion}</span>
                  </p>
                  <p className="single-detail">
                    Capital: <span>{singleCountryData[0].capital}</span>
                  </p>
                </div>
                <div className="details-right">
                  <p className="single-detail">
                    Top Level Domain: <span>{singleCountryData[0].tld}</span>
                  </p>
                  <p className="single-detail">
                    {/* Currencies: <span>{singleCountryData[0].currencies}</span> */}
                  </p>
                  <p className="single-detail">
                    Language:
                    {Object.keys(singleCountryData[0].languages).map((key) => {
                      const lang = singleCountryData[0].languages[key];
                      return <span key={key}> {lang}, </span>;
                    })}
                  </p>
                </div>
              </div>
              <div className="border-countries-container">
                <p className="border-country-title">Border Countries</p>

                {singleCountryData[0]?.borders ? (
                  Object.keys(singleCountryData[0].borders).map((key) => {
                    const brCountry = singleCountryData[0].borders[key];
                    return (
                      <p className="border-country" key={key}>
                        {brCountry}
                      </p>
                    );
                  })
                ) : (
                  <>""</>
                )}
              </div>
            </div>
          </div>
          </div>
        )}
      
    </div>
  );
}
export default CountryDetails;
