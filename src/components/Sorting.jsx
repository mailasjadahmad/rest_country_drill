import React from "react";
import { useTheme } from "../ThemeContext";
import Box from "@mui/material/Box";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
// import "./Sorting.css";

function Sorting({ onSortingFilterChange }) {
  const { darkMode } = useTheme();
  const [sortingEl, setSrotingEl] = React.useState("");

  function handlDropdownChange(event) {
    setSrotingEl(event.target.value);
    const selectedValue = event.target.value;
    onSortingFilterChange(selectedValue);
  }
  return (
    <>
      <Box sx={{  minWidth:150  }}>
        <FormControl fullWidth>
          <InputLabel id="demo-simple-select-label">Sort By</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={sortingEl}
            label="Sort By"
            onChange={handlDropdownChange}
          >
            {/* <MenuItem value="none">Sort by</MenuItem> */}
            <MenuItem value="Population Asc">population Asc</MenuItem>
            <MenuItem value="Population Desc">population Desc</MenuItem>
            <MenuItem value="Area Asc">Area Asc</MenuItem>
            <MenuItem value="Area Desc">Area Desc</MenuItem>
          </Select>
        </FormControl>
      </Box>
    </>
  );
}
export default Sorting;
