import React from "react";
import { useTheme } from "../ThemeContext";

import { MenuItem,Box,InputLabel,FormControl,Select } from '@mui/material'

function FilterSubregion({ subRegion, onFIlterSubregionChange }) {
  const { darkMode } = useTheme();
  const [subRegionForInput, setSubRegionForInput] = React.useState("");

  // console.log(subRegion)
  function handleDropdownChange(event) {
    setSubRegionForInput(event.target.value);
    const selectedValue = event.target.value;
    onFIlterSubregionChange(selectedValue);
  }
  return (
    <>
      <Box sx={{  minWidth:180 }}>
        <FormControl fullWidth>
          <InputLabel id="demo-simple-select-label">Select SubRegion</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={subRegionForInput}
            label="Select Region"
            onChange={handleDropdownChange}
          >
            <MenuItem value="" key="none">
              Select Subregion
            </MenuItem>
            {subRegion.map((item) => {
              // console.log(item)
              return (
                <MenuItem value={item} key={item}>
                  {item}
                </MenuItem>
              );
            })}
          </Select>
        </FormControl>
      </Box>
    </>
  );
  // subRegion=[];
}
export default FilterSubregion;
