import React from "react";
// import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
// import { faMagnifyingGlass } from "@fortawesome/free-solid-svg-icons";
import SearchIcon from "@mui/icons-material/Search";
import TextField from "@mui/material/TextField";
import { useTheme } from "../ThemeContext";
// import "./Search.css";

function Search({ onSearchChange }) {
  const { darkMode } = useTheme();
  const handleChange = (event) => {
    const value = event.target.value;
    onSearchChange(value);
  };
  return (
    <>
      <TextField
      sx={{minWidth:300}}
        fullWidth
        id="outlined-none"
        label="Search"
        variant="outlined"
        onChange={handleChange}
      />
    </>
  );
}

export default Search;
