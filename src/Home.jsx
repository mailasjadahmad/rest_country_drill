import React, { useEffect, useState } from "react";
import Flag from "./components/Flag";
import Header from "./components/Header";
import Filter from "./components/Filter";
import Search from "./components/Search";
import FilterSubregion from "./components/FilterSubregion";
import Sorting from "./components/Sorting";
import { useTheme } from "./ThemeContext";
import { createTheme, ThemeProvider } from "@mui/material/styles";

import "./Home.css";

function Home() {
  const { darkMode } = useTheme();
  const [subRegionArr, setSubregionArr] = useState([]);
  const [searchText, setSearchText] = useState("");
  const [countryData, setCountryData] = useState({});
  const [regionSubregionPair, setRegionSubregionPair] = useState([]);
  const [selectedRegion, setSelectedRegion] = useState("All Region");
  const [selectedSubregion, SetSelectedSubregion] = useState("");
  const [sortBy, setSortBy] = useState("none");

  const theme = createTheme({
    palette: {
      mode: darkMode ? "dark" : "light",
    },
  });

  const url = "https://restcountries.com/v3.1/all";

  useEffect(() => {
    fetch(url)
      .then((Response) => {
        return Response.json();
      })
      .then((data) => {
        // console.log(data)
        const newData = {};
        for (let i = 0; i < data.length; i++) {
          regionSubregionPair.push([data[i].region, data[i].subregion]);
          newData[data[i].name.official] = {
            countryName: data[i].name.official,
            population: data[i].population,
            region: data[i].region,
            capital: data[i].capital,
            flag: data[i].flags.png,
            key: i,
            id: data[i].cca2,
            subRegion: data[i].subregion,
            area: data[i].area,
          };
        }
        setRegionSubregionPair(regionSubregionPair);
        setCountryData(newData);
      })
      .catch((error) => {
        console.error("Error fetching data:", error);
      });
  }, []);
  function handleSearchChange(value) {
    setSearchText(value);
  }
  function handleFilterValue(value) {
    // console.log(subRegionOfaRegion);
    setSubregionArr([]);
    SetSelectedSubregion("");
    setSelectedRegion(value);
    setSortBy("none");
  }
  function handleFilterSubregion(value) {
    SetSelectedSubregion(value);
    console.log(value);
  }
  function handleFilterSorting(value) {
    const countryArray = Object.values(countryData);
    if (value == "none") {
      setCountryData(countryData);
      console.log(value);
    } else if (value == "Population Desc") {
      const sortedByPopulation = countryArray.sort((a, b) => {
        return parseInt(b.population) - parseInt(a.population);
      });

      setCountryData(sortedByPopulation, countryData);
    } else if (value == "Population Asc") {
      const sortedByPopulation = countryArray.sort((a, b) => {
        return parseInt(a.population) - parseInt(b.population);
      });

      setCountryData(sortedByPopulation, countryData);
    } else if (value == "Area Asc") {
      const sortedByArea = countryArray.sort((a, b) => {
        // Extract numeric part and convert to integer
        const areaA = parseInt(a.area);
        const areaB = parseInt(b.area);
        // Compare areas
        if (areaA < areaB) {
          return -1;
        }
        if (areaA > areaB) {
          return 1;
        }
        return 0;
      });
      setCountryData(sortedByArea);
    } else if (value == "Area Desc") {
      const sortedByArea = countryArray.sort((a, b) => {
        // Extract numeric part and convert to integer
        const areaA = parseInt(a.area);
        const areaB = parseInt(b.area);
        // Compare areas
        if (areaA > areaB) {
          return -1;
        }
        if (areaA < areaB) {
          return 1;
        }
        return 0;
      });
      setCountryData(sortedByArea);
    }

    setSortBy(value);
  }

  // let subRegion = new Set();

  const themeStyle = {
    backgroundColor: darkMode ? "#252525" : "white",
  };

  return (
    <ThemeProvider theme={theme}>
      <div className="body-bg" style={themeStyle}>
        <div className="home-page-body">
          <div className="filtering-container">
            <Search
              className="search-input"
              onSearchChange={handleSearchChange}
            />
            <div className="filters">
              <Filter onFilterChange={handleFilterValue} />
              <FilterSubregion
                subRegion={subRegionArr}
                onFIlterSubregionChange={handleFilterSubregion}
              />
              <Sorting onSortingFilterChange={handleFilterSorting} />
            </div>
          </div>
          <div className="flag-container">
            {Object.keys(countryData).length === 0 ? (
              <p>Loading...</p>
            ) : (
              Object.keys(countryData).map((countryName) => {
                const singleFlag = countryData[countryName];

                if (
                  searchText == "" ||
                  singleFlag.countryName
                    .toLowerCase()
                    .includes(searchText.toLowerCase())
                ) {
                  // console.log(searchText)
                  if (
                    selectedRegion == "All Region" ||
                    singleFlag.region
                      .toLowerCase()
                      .includes(selectedRegion.toLowerCase())
                  ) {
                    if (selectedRegion != "All Region") {
                      // console.log(singleFlag.region);
                      if (
                        selectedRegion.toLowerCase() ==
                          singleFlag.region.toLowerCase() &&
                        !subRegionArr.includes(singleFlag.subRegion)
                      ) {
                        // console.log(singleFlag.region)
                        setSubregionArr([
                          ...subRegionArr,
                          singleFlag.subRegion,
                        ]);
                      }
                      // console.log(subRegionArr);
                    }
                    if (
                      selectedSubregion == "" ||
                      selectedSubregion == singleFlag.subRegion
                    )
                      return (
                        <Flag
                          countryName={singleFlag.countryName}
                          flagImg={singleFlag.flag}
                          capital={singleFlag.capital}
                          population={singleFlag.population}
                          region={singleFlag.region}
                          key={singleFlag.key}
                          id={singleFlag.id}
                        />
                      );
                  }
                }
              })
            )}
          </div>
        </div>
      </div>
    </ThemeProvider>
  );
}

export default Home;
