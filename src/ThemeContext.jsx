// ThemeContext.js
import React, { createContext, useState, useContext } from 'react';



// Create a new context
const ThemeContext = createContext();

// Create a provider component
export const ThemeProvider = ({ children }) => {
  const [darkMode, setDarkMode] = useState(false);

  // Toggle between dark and light modes
  const toggleTheme = () => {
    setDarkMode(prevMode => !prevMode);
  };
 

  return (
    <ThemeContext.Provider value={{ darkMode, toggleTheme }} >
      {children}
    </ThemeContext.Provider>
  );
};

// Custom hook to use the theme context
export const useTheme = () => useContext(ThemeContext);
