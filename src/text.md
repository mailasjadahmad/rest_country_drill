altSpellings
: 
(6) ['CY', 'Kýpros', 'Kıbrıs', 'Republic of Cyprus', 'Κυπριακή Δημοκρατία', 'Kıbrıs Cumhuriyeti']
area
: 
9251
capital
: 
['Nicosia']
capitalInfo
: 
{latlng: Array(2)}
car
: 
{signs: Array(1), side: 'left'}
cca2
: 
"CY"
cca3
: 
"CYP"
ccn3
: 
"196"
cioc
: 
"CYP"
coatOfArms
: 
{png: 'https://mainfacts.com/media/images/coats_of_arms/cy.png', svg: 'https://mainfacts.com/media/images/coats_of_arms/cy.svg'}
continents
: 
['Europe']
currencies
: 
{EUR: {…}}
demonyms
: 
{eng: {…}, fra: {…}}
fifa
: 
"CYP"
flag
: 
"🇨🇾"
flags
: 
{png: 'https://flagcdn.com/w320/cy.png', svg: 'https://flagcdn.com/cy.svg', alt: 'The flag of Cyprus has a white field, at the cente…ove two green olive branches crossed at the stem.'}
gini
: 
{2018: 32.7}
idd
: 
{root: '+3', suffixes: Array(1)}
independent
: 
true
landlocked
: 
false
languages
: 
{ell: 'Greek', tur: 'Turkish'}
latlng
: 
(2) [35, 33]
maps
: 
{googleMaps: 'https://goo.gl/maps/77hPBRdLid8yD5Bm7', openStreetMaps: 'https://www.openstreetmap.org/relation/307787'}
name
: 
{common: 'Cyprus', official: 'Republic of Cyprus', nativeName: {…}}
population
: 
1207361
postalCode
: 
{format: '####', regex: '^(\\d{4})$'}
region
: 
"Europe"
startOfWeek
: 
"monday"
status
: 
"officially-assigned"
subregion
: 
"Southern Europe"
timezones
: 
['UTC+02:00']
tld
: 
['.cy']
translations
: 
{ara: {…}, bre: {…}, ces: {…}, cym: {…}, deu: {…}, …}
unMember
: 
true